# -*- encoding: utf-8 -*-
from django.contrib import admin
from models import Profile, Store, Quiz, Product, Answer

class ProductInline(admin.StackedInline):
    model = Product

class QuizAdmin(admin.ModelAdmin):
    inlines = [
        ProductInline
    ]

admin.site.register(Profile)
admin.site.register(Store)
admin.site.register(Quiz, QuizAdmin)
admin.site.register(Product)
