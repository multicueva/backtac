# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
import os
import base64
import cStringIO
from datetime import datetime
from django.conf import settings
from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    name = models.CharField("Nombre", max_length=200)
    address = models.CharField("Dirección", max_length=200)
    related_admin = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    def __unicode__(self):
        return self.name
    def __str__(self):
        return self.name
    class Meta:
		verbose_name = "Perfil"
		verbose_name_plural = "Perfiles"

class Quiz(models.Model):
    title = models.CharField("Titulo", max_length=50)
    def __unicode__(self):
        return self.title
    def __str__(self):
        return self.title
    class Meta:
		verbose_name = "Cuestionario"
		verbose_name_plural = "Cuestionarios"

class Store(models.Model):
    name = models.CharField("Nombre de tienda", max_length=200, blank=True, null=True)
    in_charge = models.CharField("Quién Atiende", max_length=200, blank=True, null=True)
    owner = models.CharField("Dueño de tienda", max_length=200, blank=True, null=True)
    address = models.CharField("Dirección", max_length=200, blank=True, null=True)
    city = models.CharField("Ciudad", max_length=200, null=True, blank=True)
    state = models.CharField("Estado o Provincia", max_length=200, null=True, blank=True, default="")
    area = models.CharField("Area (opcional)", max_length=200, null=True, blank=True, default="")
    telephone = models.CharField("Telefono", max_length=20, blank=True, null=True)
    comment = models.TextField("Comentarios", blank=True, null=True)
    latitude = models.FloatField("Latitud", null=True, blank=True)
    longitude = models.FloatField("Longitud", null=True, blank=True)
    related_evaluator = models.ForeignKey(Profile, blank=True, null=True, verbose_name="Evaluador", related_query_name="store", related_name="stores")
    related_quiz = models.ForeignKey(Quiz, blank=True, null=True, verbose_name="Cuestionario", related_query_name="store", related_name="stores")
    date_created = models.DateTimeField("Fecha creacion", editable=False, auto_now_add=True)
    date_updated = models.DateTimeField("Fecha modificacion", editable=False, auto_now=True)
    pop_image = models.TextField("Imagen POP")
    pop_type = models.CharField("Tipo POP", max_length=20, blank=True, null=True)
    store_image = models.TextField("Imagen Tienda")
    def __unicode__(self):
        return self.name
    def __str__(self):
        return self.name
    class Meta:
		verbose_name = "Tienda"
		verbose_name_plural = "Tiendas"

class Product(models.Model):
    title = models.CharField("Tiulo de pregunta", max_length=100)
    related_quiz = models.ForeignKey(Quiz, verbose_name="Cuestionario", on_delete=models.CASCADE, related_query_name="product", related_name="products")
    def __unicode__(self):
        return self.title
    def __str__(self):
        return self.title
    class Meta:
		verbose_name = "Producto"
		verbose_name_plural = "Productos"

class Answer(models.Model):
    stock = models.BooleanField("Hay Stock?", default=False)
    pop = models.BooleanField("Hay Pop?", default=False)
    has_flangers = models.BooleanField("Hay Chispas Afiches", default=False)
    has_sparks = models.BooleanField("Hay Flangers/Tackers", default=False)
    image = models.TextField("Imagen POP", blank=True, null=True)
    published_price = models.PositiveIntegerField("Precio Publicado", default=0)
    date_inserted = models.DateTimeField("Fecha de respuesta", auto_now=True)
    related_store = models.ForeignKey(Store, verbose_name="Tienda",  blank=True, null=True, on_delete=models.CASCADE, related_query_name="answer", related_name="answers")
    related_product = models.ForeignKey(Product, verbose_name="Respuesta", on_delete=models.CASCADE, related_query_name="answer", related_name="answers")
    date_created = models.DateTimeField("Fecha creacion", editable=False, auto_now_add=True)
    date_updated = models.DateTimeField("Fecha modificacion", editable=False, auto_now=True)
    class Meta:
        verbose_name = "Respuesta"
        verbose_name_plural = "Respuestas"
