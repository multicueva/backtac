from .models import Profile, Store, Quiz, Product, Answer
from rest_framework import serializers

class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ('__all__')

class AnswerShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ('id', 'stock', 'pop', 'published_price', 'related_store', 'image', 'has_flangers', 'has_sparks')

class ShortProductSerializer(serializers.ModelSerializer):
    answers = AnswerShortSerializer(many=True, read_only=True)
    class Meta:
        model = Product
        fields = ('__all__')

class ShortProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        lookup_field = 'id'
        fields = ('id', 'name', 'address')

class ShortQuizSerializer(serializers.ModelSerializer):
    products = ShortProductSerializer(many=True, read_only=True)
    class Meta:
        model = Quiz
        fields = ('__all__')

class StoreSerializer(serializers.ModelSerializer):
    related_quiz = ShortQuizSerializer(many=False, read_only=True)
    related_evaluator = ShortProfileSerializer(many=False, read_only=True)
    class Meta:
        model = Store
        fields = ('__all__')

class StoreWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = ('__all__')
