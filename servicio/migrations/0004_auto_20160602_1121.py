# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-02 15:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('servicio', '0003_auto_20160602_1109'),
    ]

    operations = [
        migrations.AddField(
            model_name='store',
            name='area',
            field=models.CharField(blank=True, default='', max_length=200, null=True, verbose_name='Area (opcional)'),
        ),
        migrations.AddField(
            model_name='store',
            name='state',
            field=models.CharField(blank=True, default='', max_length=200, null=True, verbose_name='Estado o Provincia'),
        ),
    ]
