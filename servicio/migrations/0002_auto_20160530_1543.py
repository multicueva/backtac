# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-30 19:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('servicio', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='store',
            name='latitude',
            field=models.FloatField(blank=True, null=True, verbose_name='Latitud'),
        ),
        migrations.AddField(
            model_name='store',
            name='longitude',
            field=models.FloatField(blank=True, null=True, verbose_name='Longitud'),
        ),
    ]
