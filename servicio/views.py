# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.db.models.expressions import RawSQL
from rest_framework.decorators import api_view, permission_classes
from rest_framework.pagination import PageNumberPagination
from rest_framework.exceptions import ParseError, NotFound, APIException
from rest_framework.response import Response
from rest_framework import status, viewsets
from .models import Profile, Store, Quiz, Product, Answer
from .serializers import StoreSerializer, ShortQuizSerializer, ShortProductSerializer, AnswerSerializer, StoreWriteSerializer

# Create your views here.
@api_view(['GET'])
def tiendas_cercanas(request):
    if 'lat' in request.query_params and 'lng' in request.query_params:
        limite_resultados = 100
        if 'limit' in request.query_params:
            if request.query_params['limit'] < 100:
                limite_resultados = int(request.query_params['limit'])
        latitud = request.query_params['lat']
        longitud = request.query_params['lng']
        query_distancia = "6371 * acos (cos ( radians(%s) )* cos( radians( `servicio_store`.`latitude` ) )* cos( radians( `servicio_store`.`longitude` ) - radians(%s) )+ sin ( radians(%s) )* sin( radians( `servicio_store`.`latitude`)))"
        tiendas = Store.objects.all().annotate(distancia=RawSQL(query_distancia, (latitud, longitud, latitud))).filter(distancia__lte=10).order_by("distancia")[:limite_resultados]
        serializer = StoreSerializer(tiendas, many=True, context={'request': request})
        return Response(serializer.data)
    else:
        return Response({'Latitud/Longitud invalida'}, 400)


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 20

class StoreViewSet(viewsets.ModelViewSet):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer
    pagination_class = StandardResultsSetPagination
    def get_serializer_class(self):
        if self.action == 'update':
            return StoreWriteSerializer
        return StoreSerializer # I dont' know what you want for create/destroy/update.
    def list(self, request):
        try:
            queryset = Store.objects.filter(related_evaluator= Profile.objects.get(related_admin=request.user))
        except Exception as e:
            return Response({"No se encontraron tiendas asociadas a este usuario"}, 400)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = StoreSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)
    def create(self, request):
        try:
            tienda = Store(
                name=request.data['name'],
                in_charge=request.data['in_charge'],
                owner=request.data['owner'],
                address=request.data['address'],
                city=request.data['city'],
                state=request.data['state'],
                area=request.data['area'],
                telephone=request.data['telephone'],
                comment=request.data['comment'],
                latitude=request.data['latitude'],
                longitude=request.data['longitude'],
                store_image=request.data['store_image'],
                pop_image=request.data['pop_image'],
                pop_type=request.data['pop_type']
            )
            quiz = Quiz.objects.get(pk=request.data['quiz_id'])
            evaluator = Profile.objects.get(related_admin=request.user)
            tienda.related_evaluator = evaluator
            tienda.related_quiz = quiz
            tienda.save()
        except Exception as e:
            raise APIException(e)
        serializer = StoreSerializer(tienda, many=False, context={'request': request})
        return Response(serializer.data)

class AnswerViewSet(viewsets.ModelViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    def list(self, request):
        try:
            queryset = Answer.objects.filter(related_profile=Profile.objects.get(related_admin=request.user))
        except Exception as e:
            return Response({"No se encontraron respuestas asociadas a este usuario"}, 400)
        serializer = StoreSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)

class QuizViewSet(viewsets.ModelViewSet):
    queryset = Quiz.objects.all()
    serializer_class = ShortQuizSerializer

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ShortProductSerializer
